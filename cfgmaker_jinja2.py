import jinja2
import csv
import sys
import os

def usage():
	print sys.argv[0]+" list dir"
	print "list     = csv file"
	print "template = template file"
	print "dir      = where output files are saved"

	
def main():		
	templateLoader = jinja2.FileSystemLoader( searchpath="./" )
	templateEnv = jinja2.Environment( loader=templateLoader )
	TEMPLATE_FILE = sys.argv[2]
	template = templateEnv.get_template( TEMPLATE_FILE )
	
	dir = sys.argv[3]
	if not os.path.exists(dir):
		os.mkdir(dir)
	
	
	print "START"
	with open(sys.argv[1], 'rt') as f:
		reader = csv.reader(f)
		for row in reader:
			if not row[0][0] == '#':
				templateVars = { "hostname" : row[0],
								"ip" : row[1],
								"netmask" : row[2],
								"username" : row[3],
								"password" : row[4]
								}
				outputText = template.render( templateVars )
				filename = dir+"\\"+row[0]
				file = open(filename,'w')
				file.write(outputText)
				file.close()
				print "CREATING CONFIG FOR "+str(row[0])
	print "DONE"	
	
	
if len(sys.argv)<3:
	usage()
else:
	main()
 
